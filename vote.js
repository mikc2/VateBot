console.log('start');
const puppeteer = require('puppeteer');
const visible = false;

const vote = async (thread_id) => {
    let startTime = Date.now();
    console.log(`thread_id: ${thread_id}`);
    try {
        const browser = await puppeteer.launch({
            args: ['--disbale-features=site-pre-process'],
            headless: !visible,
            devtools: false,
            slowMo: 50
        });

        const window = await browser.createIncognitoBrowserContext();
        const page = await window.newPage();
        await page.setViewport({width: 2000, height: 1500});
        await page.goto('https://hk.news.yahoo.com/poll/71fb4020-be60-11e9-b892-53d44f8bde65');

        const confirmButton = await page.mainFrame().$(`[value="confirm"]`);

        let yesSelector = `input[value="o0"]`;
        await page.evaluate((yesSelector) => document.querySelector(yesSelector).click(), yesSelector)
            .then(console.log("voted YES!"));

        await confirmButton.click()
            .then(console.log("submitted"));

        // let timestamp = Math.floor(Date.now() / 1000);
        // await page.waitFor(2 * 1000);
        // await page.screenshot({path: `cap/voted_finished_${timestamp}.png`});
        await browser.close();
        let endTime = Date.now();
        console.log(`spend time: ${(endTime - startTime) / 1000}s`);
        return 1;
    } catch (e) {
        console.log(e);
    }
    return 0;
};

const loop = async () => {
    let success_times = 0;
    while (true) {
        let parallelLines = 5;
        let arr = [];
        for (let i = 0; i < parallelLines; i++) {
            arr.push(vote(i));
        }
        await Promise.all(arr).then(r => success_times += r.reduce((acc, curr) => acc + curr, 0));
        console.log(`success_times: ${success_times}`);
    }
};

loop();